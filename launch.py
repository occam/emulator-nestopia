import os
import subprocess
import json

from occam import Occam

# Gather paths
scripts_path   = os.path.dirname(__file__)
emulator_path  = "%s/src" % (scripts_path)
binary         = "sh %s/start.sh" % (scripts_path)
job_path       = os.getcwd()

object = Occam.load()

# Open object.json for command line options
# data = object.configuration("General")

# Add mounts for every object
inputs = object.inputs()
for input in inputs:
  path = input.volume()
  file = input._object.get('run', {}).get('file')

# Form arguments
args = [binary,
        os.path.join(scripts_path, "default.conf")]

# Form command line
command = ' '.join(args)

# Tell OCCAM how to run DRAMSim2
Occam.report(command, env={"LD_LIBRARY_PATH": emulator_path})
